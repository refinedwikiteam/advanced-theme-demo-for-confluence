
@echo off

:: Refined Theme Uploader
:: Variables
SET nameKey=demotheme
SET username=admin
SET password=admin
SET host=http://localhost:1990/confluence

::Package the theme

::Since Windows doesn't have a proper zip command we use java
echo Compressing files...
jar -cMf %nameKey%.zip .

::Upload the new theme
echo Upload Started
curl.exe -sS -u %username%:%password% -X POST -H "Content-Type: multipart/form-data" -F "file=@%nameKey%.zip" %host%/admin/themes/import-and-replace-theme.action

echo Upload Complete
