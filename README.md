# README #

The purpose of the Advanced Theme Demo is to demonstrate how you
can create custom themes for Refined for Confluence.

You can develop custom themes with your favourite IDE or with the online
advanced theme editor included in Refined for Confluence.

Please note that you need to run Refined 5.0 for Confluence or later.

Do you want to use SASS? Check out this branch https://bitbucket.org/refinedwikiteam/advanced-theme-demo-for-confluence/src/sass

Check out the file *js/script.js* for a demo on how to use the [RefinedWiki JavaScript events](https://docs.refinedwiki.com/x/0wWCAg)


### Develop with IDE: ###

1. Open this project in your editor.
2. Make sure the settings in theme-uploader.sh/theme-uploader.cmd are valid.
3. Make changes to your theme.
4. Make sure that you have enabled external theme import. Navigate in Confluence to: Config > Theme Configurations > Themes > Advanced Themes and click on "Enable".
5. Follwo the steps below depending on your environment. 

Mac & linux:

1. Open a terminal and navigate to the folder where your Advanced Theme is located. 
2. Automatically upload your theme by running ./theme-uploader.sh (if not executable run: "chmod +x theme-uploader.sh")

Windows

1. Open a Command Prompt and navigate the folder where your Advanced Theme is located. 
2. Automatically upload your theme by running theme-uploader.cmd from you


Your theme is now uploaded to confluence. In Confluence navigate to: Config > Theme Configurations > Themes > Advanced Themes

Please note that you can also manually upload the zip file if you don't want to enable the "external theme import" on you instance. Navigate in Confluence to: Config > Theme Configurations > Themes > Advanced Themes and import theme.

### End of life notice ###
In Refined 7.0, Advanced Themes with Resource Base 1.0 is no longer supported. Please use 2.0 instead.   
