#
# Refined Theme Uploader
#


# Variables
NAME_KEY=demotheme # Must be same nameKey as in config.xml
USERNAME=admin
PASSWORD=admin
HOST=http://localhost:1990/confluence

# Package the theme
rm $NAME_KEY.zip
zip -r $NAME_KEY.zip * --exclude \*.sh --exclude \*.iml --exclude \*.cmd

# Upload the new theme
curl -sS -u $USERNAME:$PASSWORD -X POST -H "Content-Type: multipart/form-data" -F "file=@$NAME_KEY.zip" $HOST/admin/themes/import-and-replace-theme.action

