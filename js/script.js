(function($) {


  /**
   * This example will hook in on RefinedTheme javascript events to find a HTML-module containing the following:
   *
   <div class="date-fact-wrapper">
    <!-- Quote element, hidden on start. We inject data here with js -->
    <q class="fact-of-the-day" style="display:none"></q>
   </div>
   *
   * It will then fetch and inject some data to these type of categories.
   */
  AJS.toInit(function() {
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;

    // Triggers when the category dropdown is opened. At this stage the $el will contain all category elements that
    // will be populated with data.
    // If the user is using collapsed subcategories, these elements will only contain the expandable header of the
    // category. In those cases you could listen to rw-categories:category-opened and rw-categories:category-closed to
    // get info on when the collapsed categories are toggled.
    AJS.bind('rw-categories:category-dropdown-open', function(e, $el, category) {
      console.debug('Dropdown open');
      // Dropdown was open, we check if we can find our wrapper. If not, it might not be in this category, or we're
      // using collapsed mode.
      if ($('.date-fact-wrapper', $el).length > 0) {
        var $wrappingCategory = $('.date-fact-wrapper').closest('.rw_dd_category');
        fetchAndInjectData($wrappingCategory, category);
      }
    });

    // Triggered when a collapsable (sub)category is opened within the category dropdown.
    // Opening a subcategory will load and render it's content. If the category previously
    // has loaded the content is just showed as it's already present in the DOM.
    AJS.bind('rw-categories:category-opened', function(e, $el, category) {
      console.debug('Collapsed category open');
      // Category was opended from collapsed mode.
      fetchAndInjectData($el, category);
    });


    /**
     * Fetches some interesting random facts about the current date and displays it in the category.
     * Data from http://numbersapi.com/
     *
     * @param $categoryElement
     * @param category
     */
    function fetchAndInjectData($categoryElement, category) {
      // Show loader while fetching.
      $('.date-fact-wrapper', $categoryElement).spin();
      var $quoteWrapper = $('.fact-of-the-day', $categoryElement);

      // Fetch data for this day.
      $.ajax({
        cache: false, // To prevent same fact the entire day for IE-users
        type: 'GET',
        url: 'http://numbersapi.com/' + month + '/' + day + '/date',
        success: function(data, textStatus, jqXHR) {
          // Print the data
          $quoteWrapper.html(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          $quoteWrapper.html('An error occured while fetching facts for this date.')
          console.error(textStatus, errorThrown);
        }
      }).always(function(jqXHR, textStatus, errorThrown) {
        // When done, stop loader and show data.
        $('.date-fact-wrapper', $categoryElement).spinStop();
        $quoteWrapper.show();
      });
    }
  });


})(AJS.$);
